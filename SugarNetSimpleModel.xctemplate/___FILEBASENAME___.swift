//___FILEHEADER___ //___FILEBASENAMEASIDENTIFIER___

// more information https://gitee.com/tanpengsccd/TPXcodeCocoaTouchTemplate.git

import Sugar_NetWorking
import Alamofire
import HandyJSON
//简单数据

class ___FILEBASENAMEASIDENTIFIER___:SugarNetSimpleModelProtocol {
    
    //MARK: - properties
    ///必须 设置之一（不能都为nil）： 1  baseURL 2 Sugar_NetWorking_Configuration.sharedDefault.uri
    var baseURL: String? = nil
    ///✨ 参数path
    var path = //✨
    
    ///✨请求方式
    var method: HTTPMethod? =
    ///请求属性
    var model_req = Req.init()
    //响应属性
    var model_res: Res?
    
    //MARK: - method
    func didRespond(newRes: ___FILEBASENAMEASIDENTIFIER___.Res) {
        //可再做点其它操作
        
        //以下一般不修改
        self.model_res = newRes
    }
    
    
    
    //MARK:- 请求 和 返回 模型
    typealias SugarNetBaseModelReqT = Req
    typealias SugarNetBaseModelResT = Res
    ///请求模型
    class Req:SugarNetSimpleModelReq{
        //✨此处申明请求参数
        
        
        required init() {
            
        }
    }
    
    ///响应模型
    class Res:SugarNetSimpleModelRes{
        var code: Int?
        
        var msg: String?
        
        var status: String?
        
        var data: ___FILEBASENAMEASIDENTIFIER___.Res.Data?
        
        
        
        typealias SugarNetBaseModelResDataT = Data
        
        class  Data: SugarNetSimpleModelResData{
            //✨此处响应的具体参数
            
            required init(){}
        }
        required init(){}
    }
}



