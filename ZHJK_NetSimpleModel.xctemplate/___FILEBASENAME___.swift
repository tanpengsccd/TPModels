//___FILEHEADER___

// more information https://gitee.com/tanpengsccd/TPXcodeCocoaTouchTemplate.git

import HandyJSON
import ECT_NetWorking
import Alamofire
class ___FILEBASENAMEASIDENTIFIER___:ECT_NetSimpleModel_Protocol{
    
    //MARK: - properties
    ///必须 设置之一（不能都为nil）： 1  baseURL 2 ZHJK_NetWorking_Configuration.sharedDefault.uri
    var baseURL: String? = nil
    ///✨ 参数path
    var path =
    
    ///✨请求方式
    var method: HTTPMethod? = .get
    ///请求属性
    var model_req = Req.init()
    ///响应属性
    var model_res: Res?
    
    //MARK: - method
    func didRespond(newRes: ___FILEBASENAMEASIDENTIFIER___.Res) {
        //可再做点其它操作
        
        //以下一般不修改
        self.model_res = newRes
    }
    
    //MARK:- 请求 和 返回 模型
    ///请求模型
    class Req:ECT_NetSimpleModel_Req{
        //✨此处申明请求参数

        
        required init() {
        }
    }
    ///响应模型
    class Res:ECT_NetSimpleModel_Res{
        
        class Data : ECT_NetSimpleModel_Res_Data {
            //✨此处响应的具体参数
            
            required init(){}
        }
        class Result:ECT_NetSimpleModel_Res_Result{
            var code : Int?
            var desc : String?
            required init(){}
        }
        var result: Result?
        var data: Data?
        var validResult: String?
        
        required init() {
        }
        
    }
    
}




