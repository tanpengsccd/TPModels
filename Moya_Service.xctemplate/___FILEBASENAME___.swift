//___FILEHEADER___

import Moya
import Foundation


//MARK: -  任务（订单）
enum ___FILEBASENAMEASIDENTIFIER___ {
    case examle(_:TP_Swift_Model.Req)
    
}

extension ___FILEBASENAMEASIDENTIFIER___ : TargetType{
    
    
    
    var headers: [String : String]? {return ["token":UserDefaults.standard.string(forKey: USERTOKEN)!,"Content-Type": "application/json;charset=UTF-8"]}
    var baseURL : URL{ return URL.init(string: Domain_EasyCarTreasure.substring(from: 0) + "/vasms-web")! }
    
    //
    var path : String{
        switch self {
            //case (let req): return "api/v1/org/employeeInfo/statistics/all"
            
        }

    }
    
    var method: Moya.Method{
        switch self {
//        case .examle:
//            return .get
            
        }
        
    }
    
    var task: Task{
        switch self {
        case let .examle(req):
            return .requestParameters(parameters: req.toJSON()! as [String:Any], encoding: URLEncoding.queryString)
//        case let .examle(req):
//            return .requestCompositeParameters(bodyParameters: req.toJSON()! as [String:Any], bodyEncoding: JSONEncoding.default,  urlParameters: [:])
        }
    }
    
    var sampleData: Data {
        switch self {
//            case .examle(_):
//                guard let url = Bundle.main.url(forResource: "example", withExtension: "json"),let data = try? Data(contentsOf: url) else {
//                        return Data()
//                }
//                return data
            
            default: return Data()
        }
    }
    
}


