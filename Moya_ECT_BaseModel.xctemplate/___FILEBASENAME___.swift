//___FILEHEADER___
import HandyJSON

class ___FILEBASENAMEASIDENTIFIER___ :TP_Swift_ECT_BaseModel{
    // requset 模型,继承 HandyJson
    class Req:TP_Swift_ECT_BaseModel_req{
        //请求参数模型
        var example
        
        // model 映射关系自定义
        required init() {}
        override func mapping(mapper: HelpingMapper) {
            //            mapper >>> self.example
        }
    }
    //response 模型,继承 HandyJson
    class Res:TP_Swift_ECT_BaseModel_res{
        class Data:HandyJSON{
            var example

            required init() {
                
            }
        }
        var data:Data?
        required init() {}
        override func mapping(mapper: HelpingMapper) {
//            mapper >>> self.example
        }
    }
    
    
    var req = Req()
    var res : Res?
    
}




