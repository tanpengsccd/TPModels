//___FILEHEADER___

// more information https://gitee.com/tanpengsccd/TPXcodeCocoaTouchTemplate.git


import Sugar_NetWorking
import Alamofire
import HandyJSON

// 分页数组数据

class ___FILEBASENAMEASIDENTIFIER___:SugarNetPagingModelProtocol {
    
    //MARK: - properties
    ///必须 设置之一（不能都为nil）： 1  baseURL 2 Sugar_NetWorking_Configuration.sharedDefault.uri
    var baseURL: String? = nil
    ///✨ 参数path
    var path: String =
    
    ///✨请求方式
    var method: HTTPMethod? =
    ///请求属性
    var model_req = Req.init()
    //响应属性
    var model_res: Res?
    
    func didRespond(newRes: ___FILEBASENAMEASIDENTIFIER___.Res) {
        
    }
    
    
    //MARK:- 请求 和 返回 模型
    typealias SugarNetBaseModelReqT = Req
    typealias SugarNetBaseModelResT = Res
    
    ///请求模型
    class Req:SugarNetPagingModelReq{
        
        var page: Int? = 1
        var size: Int? = 20
        //✨此处申明请求参数

        
        required init() {
            
        }
    }
    
    ///响应模型
    class Res:SugarNetPagingModelRes{
        var code: String?
        
        var message: String?
        
        var status: String?
        
        var data: ___FILEBASENAMEASIDENTIFIER___.Res.Data?
        
        typealias SugarNetBaseModelResDataT = Data
        
        class Data:SugarNetPagingModelResData{
            var page: Int?
            
            var size: Int?
            
            var total: Int?
            
            var pages: Int?
            
            var list = [___FILEBASENAMEASIDENTIFIER___.Res.Data.ListElementT]()
            
            typealias SugarNetPagingModelResDataListElementT = ListElementT
            class ListElementT:SugarNetPagingModelResDataListElement{
                //✨此处响应的具体参数

                
                required init() {}
            }
            required init() {}
        }
        
        required init() {}
    }
    
}

