//___FILEHEADER___

import Foundation
import ECT_NetWorking
import Alamofire
import HandyJSON
class ___FILEBASENAMEASIDENTIFIER___ :ECT_NetPagingModel_Protocol{
    
    //MARK: - properties
    ///必须 设置之一（不能都为nil）： 1  baseURL 2 ZHJK_NetWorking_Configuration.sharedDefault.uri
    var baseURL: String? = nil
    ///✨ 参数path
    var path =
    ///✨请求方式
    var method: HTTPMethod? = .get
    ///请求属性
    var model_req = Req.init()
    ///响应属性
    var model_res: Res?
    
    //MARK: - ECT_NetSimpleModel_Protocol 协议方法
    func didRespond(newRes: ___FILEBASENAMEASIDENTIFIER___.Res) {
        //可再做点其它操作
        
        
        //
        let res = self.getResWithMergedRecords(newRes: newRes)
        self.model_res = res
        
    }
    
    //MARK:- 具体请求 响应
    ///请求模型
    class Req:ECT_NetPagingModel_Req{
        var showCount: Int? = 20
        var currentPage: Int? = 1
        //✨请求参数

        
        
        required init() {
        }
    }
    ///响应模型
    class Res:ECT_NetPagingModel_Res{
        
        class Data : ECT_NetPagingModel_Res_Data {
            
            class Record:ECT_NetPagingModel_Res_Data_Record{
                //✨ 返回的属性
                
                required init(){}
            }
            var showCount : Int?
            var totalPage : Int?
            var totalResult : Int?
            var currentPage : Int?
            var records:[Record] = []
            required init(){}
        }
        class Result:ECT_NetPagingModel_Res_Result{
            var code : Int?
            var desc : String?
            required init(){}
        }
        var result: Result?
        var data: Data?
        var validResult: String?
        required init() {}
        
        
        
    }
    
}




