//___FILEHEADER___
import HandyJSON

class ___FILEBASENAMEASIDENTIFIER___ :TP_Swift_ECT_PagingModel{
    // requset 模型,继承 HandyJson
    class Req:TP_Swift_ECT_PagingModel_req{
        //请求参数模型
        var example
        
        // model 映射关系自定义
        required init() {}
        override func mapping(mapper: HelpingMapper) {
            //            mapper >>> self.example
        }
    }
    //response 模型,继承 HandyJson
    class Res:TP_Swift_ECT_PagingModel_res{
        class Data:Data_paging{
            class Record: HandyJSON {
                // 返回的分页数据模型，可嵌套
                var example
                required init() {}
                func mapping(mapper: HelpingMapper) {
                    //            mapper >>> self.example
                }
            }
            var records: [Record] = []
            
            required init() {}
            func mapping(mapper: HelpingMapper) {
                //            mapper >>> self.example
            }
        }
        var data:Data?
        required init() {}
        override func mapping(mapper: HelpingMapper) {
//            mapper >>> self.example
        }
    }
    
    
    var req = Req()
    var res : Res?
    
}




